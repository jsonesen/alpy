"""Pyest configurations and logile generation script."""
import pytest


def pytest_addoption(parser):
    """Add options for performance testing."""
    parser.addoption('--performance', action='store_true', default=False,
                     help='Option to generate large log file testing speed',
                     )


def generate_log(filename, linecount=1000000):
    """Generate Apache log file to test file read performance."""
    logstring = '127.0.0.1 - foo [10/Oct/2000:13:55:36 -0700] "GET /test.ts' +\
        't HTTP/1.0" 200 2326'
    with open(filename, 'w') as f:
        for i in range(linecount):
            f.writelines([' '.join([logstring for i in all]), '\n'])


def pytest_collection_modifyitems(config, items):
    """Pytest builtin that will modify a collection of tests based on marks."""
    if config.getoption('--performance'):
        return
    skip_test = pytest.mark.skip(reason='requires performance flag')
    for item in items:
        if 'performance' in item.keywords:
            item.add_marker(skip_test)
