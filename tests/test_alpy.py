"""Test basic functionality of Alpy."""
import subprocess

from alpy import cli

parser = cli.init_parser()


def test_args():
    """Tests default args."""
    parser.parse_args((
        '--logfile', 'tests/data/logfile.txt',
        '--operation', 'gt',
        '--n-records', '1000',
        '--response', '400',
        '--n-hours', '1',
        '--exit-code', '2',
    ))


def test_script_call():
    """Tests command line interface with default args."""
    subprocess.run([
        'python', '-m', 'alpy.cli',
        '--logfile', 'tests/data/logfile.txt',
        '--operation', 'gt',
        '--n-records', '1000',
        '--response', '400',
        '--n-hours', '1',
        '--exit-code', '2',
    ])
