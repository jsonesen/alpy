"""Testing alpy core cli functions and parsing logic."""
import pytest

from alpy import cli

parser = cli.init_parser()


def test_no_args():
    """Assert critical failure with no status code present."""
    with pytest.raises(SystemExit):
        parser.parse_args([])


def test_bad_args():
    """Assert critical failure with no status code present."""
    with pytest.raises(SystemExit) as excinfo:
        parser.parse_args(['tests/data/logfile_bad.txt'])
    assert excinfo.value.code == 2


def test_bad_filepath():
    """TODO: Docstring for test_bad_filepath.

    :function: Test bad file pathname

    """
    with pytest.raises(SystemExit) as excinfo:
        parser.parse_args(['baz.txt'])
    assert excinfo.value.code == 2


def test_bad_host():
    """Test bad log host expecting ValueError with IPv in the message."""
    with pytest.raises(ValueError) as excinfo:
        cli._parselogline('FOO BAR - frank [10/Oct/2000:13:55:36 -0700]'
                          '"GET /apache_pb.gif HTTP/1.0" 200 2326')
    assert 'IPv' in excinfo.exconly()


def test_bad_time():
    """Test bad log host expecting ValueError with 'time' in message."""
    with pytest.raises(ValueError) as excinfo:
        cli._parselogline('0.0.0.0 - frank [**/Oct/2000:13:55:36 -0700]'
                          '"GET /apache_pb.gif HTTP/1.0" 200 2326')
    assert 'time' in excinfo.exconly()
