"""Arguments parser logic and CLI."""
import time
import ipaddress
import argparse


def init_parser():
    """Arg initialization."""
    parser = argparse.ArgumentParser(
        prog='alpy',
        description="""A simple script for analysing an HTTP access logs in
        Apache's Common Log Format. Alpy's default behavior is to check a given
        logfile for operation (greater than or less than) n_records with a
        given response in the last n_hours then exit with the given exit code.
        """,
    )
    parser.add_argument(
        '--logfile',
        help='http access log filepath',
        type=argparse.FileType('r'),
        required=True,
    )
    parser.add_argument(
        '--operation',
        choices=['gt', 'lt'],
        help='determine limit operation i.e less/greater than',
        type=str,
        required=True,
    )
    parser.add_argument(
        '--n-records',
        help='records rate limit min/max',
        type=int,
        required=True,
    )
    parser.add_argument(
        '--response',
        help='status code to check',
        type=int,
        required=True,
    )
    parser.add_argument(
        '--n-hours',
        help='time difference from now in whole hours',
        type=int,
        required=True,
    )
    parser.add_argument(
        '--exit-code',
        help='exit code for failed check',
        type=int,
        required=True,
    )

    return parser


def _getloglines(logfile):
    lines = []
    for line in logfile.read():
        lines.append(_parselogline(line))

    return lines


def _parselogline(line):
    line = line.split()
    return {
        'host': ipaddress.ip_address(line[0]),
        'user': line[2],
        'time': time.strptime(line[3] + line[4], '[%d/%b/%Y:%H:%M:%S%z]'),
        'request': line[5],
        'path': line[6],
        'header': line[7],
        'status': line[8],
        'size': line[9],
    }


if __name__ == '__main__':
    parser = init_parser()
    args = parser.parse_args()
    _getloglines(args.logfile)
