.. topic:: **Alpy**

    *Useful HTTP access log analytics.*

    .. image:: https://gitlab.com/eyeo/devops/playground/alpy/badges/dev/pipeline.svg
         :target: https://gitlab.com/eyeo/devops/playground/alpy/-/commits/dev
         :align: center
         :alt: pipeline status

    .. image:: https://gitlab.com/eyeo/devops/playground/alpy/badges/dev/coverage.svg
         :align: center
         :target: https://gitlab.com/eyeo/devops/playground/alpy/-/commits/dev
         :alt: coverage report


########

.. note:: This is currently in **early and exploratory** development

########

.. topic:: **But, why Alpy?**

  *Alpy is a script used to analyse application level HTTP log files in Apache
  format.*

  - **Modern-er** Python 3.7+ Support
  - **Configurability-er** status code tolerance via command line interface
  - **App level analytics-er** stuff about webapp logging
  - **Testable-er**

  Status code tolerance and rates can be hard to get analytics or statistics on
  servers with both root and app level servers running under Nagios or others.

""""


.. topic:: **Usage**

  *Alpy currently support CLI calls via python module with API access soon.*

""""

  .. code-block:: shell-session

      ➜ python -m alpy.cli --help
      usage: alpy [-h] --logfile LOGFILE --operation {gt,lt} --n-records N_RECORDS --response RESPONSE
                  --n-hours N_HOURS --exit-code EXIT_CODE

      A useful script for analysing an HTTP access logs in Apache's Common Log Format. Alpy's default behavior
      is to check a given logfile for operation (greater than or less than) n_records with a given response in
      the last n_hours then exit with the given exit code.

      optional arguments:
        -h, --help            show this help message and exit
        --logfile LOGFILE     http access log filepath
        --operation {gt,lt}   determine limit operation i.e less/greater than
        --n-records N_RECORDS
                              records rate limit min/max
        --response RESPONSE   status code to check
        --n-hours N_HOURS     time difference from now in whole hours
        --exit-code EXIT_CODE
                              exit code to indicate bad check

""""

          The goal is to support parameters where `n` percentage of status code
          `x` within `t` hours will yield warnings, criticals, or ok exit codes based on
          sensible defaults and additional checks.

""""

.. topic:: **Testing**

    *Running tests is fast with the tox tool.*


.. code-block:: shell-session

  ➜ pip install tox
  ➜ tox

""""

.. topic:: **Contributing**

    Contributions are greatly appreciated, issues can be files and new features
    should come with unit tests (see: `pytest <https://docs.pytest.org/en/stable/>`_.)
    and documentation.
