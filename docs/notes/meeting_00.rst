Alpy Design and Architecture Meeting
************************************

  - **Date:** 11/07/2020
  - **Members:** Matze and Jon nerd out on Alpy.
  - **Intent:** Make design an architecture decisions as well as document high level decisions and considerations.
  - **Pronounciation:** al-pee
  - **Follow-Up**: We will meet again next week to reiterate architecture and address.

  **Abstract**

  Alpy is a wrapper for monitoring_plugins used by icinga/nagios, the default
  and first plugins we write will work as a async logfile processors using an
  observer/parser architecture to handle the massive amounts of text data.

-----------------------------------

Components Overview
===================

.. topic:: Top level wrapper (command line interface)

  wraps monitoring_plugins in ``./alpy/plugins``  directory

  .. note::
    * allows users to express conditions and outcomes via option flags
    * passes flags to top level parser which dictates parser and observer outcomes
    * *Requires* earliest relevant timestamps from the observer

.. topic:: Top level parser (i/o for plugin scripts)

  parses log files for observation

  .. note::
    * depends on relevant timestamps based on the top level user expression for data aggregation and output for the subscribers
    * helps the observer decide which data points make a row relevant
    * up for debate/eval as to how to proceed/final outcome
        - parsing the whole file before the set of observers get activated
        - per line parsing and calling relavant observers based on the current place in the file buffer


.. topic:: Observer and Publisher Architecture

    * Observers triggered by command line arguments
    * Parsers *may* process log in one go
    * Parser should use time stamp and deltas for optimizations


________


Optimization Ideas
--------------------

  * Syncing ranges will be vital for intervals
  * earliest relevant timestamps based on desired observer can greatly reduce parser load/effort
  * *Considerations*

    Question: What happens when multiple alpy called observers resolve at once?

    Idea: Alpy should maybe run as daemon which reports to the nagios server

    * one option: Invoking an observer per line
        * *observer* decides whether a line is relevant
    * second option: Invoking observers for only relvant lines
        * *publisher* decides whether a line is relevant` 

  * Alpy is a wrapper for monitoring_plugins via icinga/nagios
     * as such it can determine which plugins can be a script or daemon
